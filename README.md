

## What is this repository for?

* Quick summary
This is a sample express application.

* Version 
0.1.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up?
* Dependencies
```console
npm install
```
* How to run tests
```console
npm test
```
* Deployment instructions
```console
npm start
```
